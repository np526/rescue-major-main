import odrive
from fibre import Logger, Event
import odrive_enums as oenums
import time
import math

oe = oenums.Errors()

class OdriveController:
    def __init__(self, name, serial_no=None):
        self.name = name
        self.serial_no = serial_no
        self.shutdown_token = Event()
        self.logger = Logger(verbose=False)
        self.device = None
        odrive.find_all(
            "usb",
            self.serial_no,
            lambda device: self.did_discover_device(device),
            self.shutdown_token,
            self.shutdown_token,
            self.logger
        )

    def is_connected(self):
        return self.device is not None

    def _calibrate(self, axis):
        if self.device:
            axis.requested_state = 3
            while axis.current_state != 1:  # Wait for calibration to be done
                time.sleep(0.1)
            axis.requested_state = 8
            if axis.current_state == 8:
                print("Calibrated!")

                # Set the ramp velocity mode to true
                axis.controller.vel_ramp_enable = True
                return True
            else:
                print("Unable to go into closed loop control")
                return False
        print("Could not find ODrive")
        return False

    def calibrate_axis0(self):
        return self._calibrate(self.device.axis0)

    def calibrate_axis1(self):
        return self._calibrate(self.device.axis1)

    def calibrate(self):
        return self.calibrate_axis0() and self.calibrate_axis1()

    def did_discover_device(self, device):
        print(self.name + ": Found an odrive!")
        self.device = device
        device.__channel__._channel_broken.subscribe(lambda: self.did_lose_device())
        my_odrive = device
        my_odrive.axis0.controller.config.vel_limit = 8192 * 100
        my_odrive.axis1.controller.config.vel_limit = 8192 * 100
        my_odrive.axis0.motor.config.current_lim = 30
        my_odrive.axis1.motor.config.current_lim = 30

    def did_lose_device(self):
        print(self.name + ": Lost an odrive!")
        self.device = None

    def get_diagnostics_message(self):
        if self.device is None:
            message = {"connected": str(False)}
        else:
            message = {
                "connected": str(True),
                "bus-voltage": str(self.device.vbus_voltage),
                "axis0-error": oe.axis[self.device.axis0.error],
                "axis0-encoder-error": oe.encoder[self.device.axis0.encoder.error],
                "axis0-motor-error": oe.motor[self.device.axis0.motor.error],
                #"axis0-controller-error": oe.controller[self.device.axis0.controller.error],
                "axis0-ctrl-mode": oe.control_modes[self.device.axis0.controller.config.control_mode],
                "axis0-state": oe.axis_states[self.device.axis0.current_state],
                "axis1-error": oe.axis[self.device.axis1.error],
                "axis1-encoder-error": oe.encoder[self.device.axis1.encoder.error],
                "axis1-motor-error": oe.motor[self.device.axis1.motor.error],
                #"axis1-controller-error": oe.errors.controller[self.device.axis1.controller.error],
                "axis1-ctrl-mode": oe.control_modes[self.device.axis1.controller.config.control_mode],
                "axis1-state": oe.axis_states[self.device.axis1.current_state],
            }

        print(repr(message))
        return message

    def _write_velocity(self, value, accEnable, axis):
        if self.device:
            axis.controller.config.control_mode = 2             # Velocity control 
            #axis.controller.config.control_mode = 2            # Control mode = INPUT_MODE_VEL_RAMP (this is for firmware v.0.5)
            
            if accEnable:
                axis.controller.vel_ramp_target = int(value)    # Set velocity (count/s) with limited acceleration
            else:
                axis.controller.vel_setpoint = int(value)       # Set velocity (counts/s)

            # Clear some errors because why not  
            axis.motor.error = 0
            axis.error = 0


            '''
            Note for future:
            for new odrive firmware change 
            1.
            vel_setpoint => input_vel (this will now be turns/s and not counts/s)

            2.
            with new firmware (v0.5), the write velocity and write velocity with acceleration enabled will be unified with input_vel. maybe... 
            '''


    def write_velocity_axis0(self, value, accEnable):
        self._write_velocity(value, accEnable, self.device.axis0)

    def write_velocity_axis1(self, value):
        self._write_velocity(value, accEnable, self.device.axis1)


    def set_vel_lim(self, val):
        self.device.axis0.controller.config.vel_limit = round((val * 8192 * gearing) / (wheel_radius*2*math.pi))
        self.device.axis1.controller.config.vel_limit = round((val * 8192 * gearing) / (wheel_radius*2*math.pi))
        

    def set_acc_lim(self, val):
        self.device.axis0.controller.config.vel_ramp_rate = round((val * 8192 * gearing) / (wheel_radius*2*math.pi))
        self.device.axis1.controller.config.vel_ramp_rate = round((val * 8192 * gearing) / (wheel_radius*2*math.pi))
        

    def set_curr_lim(self, val):
        self.device.axis0.motor.config.current_lim = val
        self.device.axis1.motor.config.current_lim = val
        

    def clear_errors(self):
        self.device.axis0.motor.error = 0
        self.device.axis1.motor.error = 0

        self.device.axis0.error = 0
        self.device.axis1.error = 0
        print("Errors should** be cleared now")


# Robot hardware parameters
gearing = 78
wheel_radius = 0.09
wheel_base = 0.32

# Maximum values for controller
max_linear_vel = 0.5
max_angular_vel = 45
max_acceleration = 2
max_current = 5

# Acceleration mode = on
acc = True


def connect_odrive():
    global odrive_controller
    odrive_controller = OdriveController("Drive")
    print("Created odrive controller object")


def drive(lin, ang):
    global odrive_controller, gearing, wheel_diameter, wheel_base, acc

    common = round( (lin * 8192 * gearing) / (wheel_radius*2*math.pi) )

    differential = round(8192 * gearing * wheel_base * ang / (wheel_radius*4*180))

    odrive_controller.write_velocity_axis0((common + differential), acc)
    odrive_controller.write_velocity_axis1(-1*(common - differential), acc)

    #print("common ", common)
    #print("differential ", differential)


import tkinter as tk
import tk_widgets as tkw

odrive_controller = None

root = tk.Tk()
root.title("ODrive Control")

frame = tk.Frame(root, width=200, height=100)
frame.grid(column=0, row=0, pady=10)

up = tkw.HoldButton(frame, text="↑", key="w", ondown=lambda e: drive(vel_lim_val.get(), 0), onup=lambda e: drive(0, 0))
down = tkw.HoldButton(frame, text="↓", key="s", ondown=lambda e: drive(-vel_lim_val.get(), 0), onup=lambda e: drive(0, 0))
left = tkw.HoldButton(frame, text="←", key="a", ondown=lambda e: drive(0, -ang_lim_val.get()), onup=lambda e: drive(0, 0))
right = tkw.HoldButton(frame, text="→", key="d", ondown=lambda e: drive(0, ang_lim_val.get()), onup=lambda e: drive(0, 0))

up.grid(row=0, column=1, sticky="nwes", ipadx=5, ipady=5, padx=5, pady=5)
down.grid(row=1, column=1, sticky="nwes", ipadx=5, ipady=5, padx=5, pady=5)
left.grid(row=1, column=0, sticky="nwes", ipadx=5, ipady=5, padx=5, pady=5)
right.grid(row=1, column=2, sticky="nwes", ipadx=5, ipady=5, padx=5, pady=5)

calibrate = tkw.HoldButton(frame, text="Calibrate", command=lambda: odrive_controller.calibrate())
calibrate.grid(row=2, column=0, sticky="nwes", ipadx=5, ipady=5, padx=5, pady=5, columnspan=3)

connect = tkw.HoldButton(frame, text="Connect", command=connect_odrive)
connect.grid(row=3, column=0, sticky="nwes", ipadx=5, ipady=5, padx=5, pady=5, columnspan=3)

clear = tkw.HoldButton(frame, text="Clear Errors", command=lambda: odrive_controller.clear_errors())
clear.grid(row=4, column=0, sticky="nwes", ipadx=5, ipady=5, padx=5, pady=5, columnspan=3)

debug = tkw.HoldButton(frame, text="Debug Message", command=lambda: odrive_controller.get_diagnostics_message())
debug.grid(row=5, column=0, sticky="nwes", ipadx=5, ipady=5, padx=5, pady=5, columnspan=3)

config = tk.Frame(root, width=300, height=100, pady=10)
config.grid(row=1, column=0)



vel_lim_val = tk.DoubleVar(value=max_linear_vel/2)

vel_lim_label = tk.Label(config, text="Linear velocity (m/s)")
vel_lim_label.grid(row=0, column=0)

vel_lim = tk.Scale(config, orient=tk.HORIZONTAL, length=200, to=max_linear_vel, resolution=max_linear_vel/100, tickinterval=max_linear_vel, variable=vel_lim_val, command=lambda e: odrive_controller.set_vel_lim(vel_lim_val.get() * 2))
vel_lim.grid(row=1, column=0, sticky="nwes", ipadx=5, ipady=5, padx=5, pady=5)




ang_lim_val = tk.DoubleVar(value=max_angular_vel/2)

ang_lim_label = tk.Label(config, text="Angular velocity (deg/s)")
ang_lim_label.grid(row=2, column=0)

ang_lim = tk.Scale(config, orient=tk.HORIZONTAL, length=200, to=max_angular_vel, resolution=max_angular_vel/100, tickinterval=max_angular_vel, variable=ang_lim_val) 
ang_lim.grid(row=3, column=0, sticky="nwes", ipadx=5, ipady=5, padx=5, pady=5)




acc_lim_val = tk.DoubleVar(value=max_acceleration/2)

acc_lim_label = tk.Label(config, text="Acceleration limit (m/s^2)")
acc_lim_label.grid(row=4, column=0)

acc_lim = tk.Scale(config, orient=tk.HORIZONTAL, length=200, to=max_acceleration, resolution=max_acceleration/100, tickinterval=max_acceleration, variable=acc_lim_val, command=lambda e: odrive_controller.set_acc_lim(acc_lim_val.get()))
acc_lim.grid(row=5, column=0, sticky="nwes", ipadx=5, ipady=5, padx=5, pady=5)





curr_lim_val = tk.DoubleVar(value=max_current/2)

curr_lim_label = tk.Label(config, text="Current limit (A)")
curr_lim_label.grid(row=6, column=0)

curr_lim = tk.Scale(config, orient=tk.HORIZONTAL, length=200, to=max_current, resolution=max_current/100, tickinterval=max_current, variable=curr_lim_val, command=lambda e: odrive_controller.set_curr_lim(curr_lim_val.get()))
curr_lim.grid(row=7, column=0, sticky="nwes", ipadx=5, ipady=5, padx=5, pady=5)

tk.mainloop()