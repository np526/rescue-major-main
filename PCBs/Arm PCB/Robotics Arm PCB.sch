EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x06 J1
U 1 1 5D96CB0E
P 750 1150
F 0 "J1" H 668 625 50  0000 C CNN
F 1 "ELB_SHO" H 668 716 50  0000 C CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43045-0612_2x03_P3.00mm_Vertical" H 750 1150 50  0001 C CNN
F 3 "~" H 750 1150 50  0001 C CNN
	1    750  1150
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J2
U 1 1 5D96E297
P 750 1950
F 0 "J2" H 668 1525 50  0000 C CNN
F 1 "BASE" H 668 1616 50  0000 C CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43650-0415_1x04_P3.00mm_Vertical" H 750 1950 50  0001 C CNN
F 3 "~" H 750 1950 50  0001 C CNN
	1    750  1950
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J3
U 1 1 5D96F372
P 750 2600
F 0 "J3" H 668 2175 50  0000 C CNN
F 1 "GRIP" H 668 2266 50  0000 C CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43650-0415_1x04_P3.00mm_Vertical" H 750 2600 50  0001 C CNN
F 3 "~" H 750 2600 50  0001 C CNN
	1    750  2600
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J4
U 1 1 5D96F802
P 750 3250
F 0 "J4" H 668 2825 50  0000 C CNN
F 1 "END_SW1" H 668 2916 50  0000 C CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43650-0415_1x04_P3.00mm_Vertical" H 750 3250 50  0001 C CNN
F 3 "~" H 750 3250 50  0001 C CNN
	1    750  3250
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J5
U 1 1 5D96FE9A
P 750 3900
F 0 "J5" H 668 3475 50  0000 C CNN
F 1 "END_SW2" H 668 3566 50  0000 C CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43650-0415_1x04_P3.00mm_Vertical" H 750 3900 50  0001 C CNN
F 3 "~" H 750 3900 50  0001 C CNN
	1    750  3900
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J6
U 1 1 5D970891
P 750 4600
F 0 "J6" H 668 4175 50  0000 C CNN
F 1 "EXTRA_ANALOG" H 668 4266 50  0000 C CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43650-0415_1x04_P3.00mm_Vertical" H 750 4600 50  0001 C CNN
F 3 "~" H 750 4600 50  0001 C CNN
	1    750  4600
	-1   0    0    1   
$EndComp
$Comp
L MotorDriver:MotorDriver U1
U 1 1 5D9712BA
P 2850 6750
F 0 "U1" H 3100 6000 50  0000 L CNN
F 1 "MotorDriver" H 2950 5900 50  0000 L CNN
F 2 "Footprints:MotorDriver" H 3150 6700 50  0001 C CNN
F 3 "" H 3150 6700 50  0001 C CNN
	1    2850 6750
	0    1    1    0   
$EndComp
$Comp
L MotorDriver:MotorDriver U2
U 1 1 5D9752AD
P 3800 6750
F 0 "U2" H 4050 6000 50  0000 L CNN
F 1 "MotorDriver" H 3900 5900 50  0000 L CNN
F 2 "Footprints:MotorDriver" H 4100 6700 50  0001 C CNN
F 3 "" H 4100 6700 50  0001 C CNN
	1    3800 6750
	0    1    1    0   
$EndComp
$Comp
L MotorDriver:MotorDriver U3
U 1 1 5D975A1F
P 4750 6750
F 0 "U3" H 5000 6000 50  0000 L CNN
F 1 "MotorDriver" H 4850 5900 50  0000 L CNN
F 2 "Footprints:MotorDriver" H 5050 6700 50  0001 C CNN
F 3 "" H 5050 6700 50  0001 C CNN
	1    4750 6750
	0    1    1    0   
$EndComp
$Comp
L MotorDriver:MotorDriver U4
U 1 1 5D975EFD
P 5700 6750
F 0 "U4" H 5950 6000 50  0000 L CNN
F 1 "MotorDriver" H 5800 5900 50  0000 L CNN
F 2 "Footprints:MotorDriver" H 6000 6700 50  0001 C CNN
F 3 "" H 6000 6700 50  0001 C CNN
	1    5700 6750
	0    1    1    0   
$EndComp
$Comp
L MotorDriver:MotorDriver U5
U 1 1 5D976430
P 6650 6750
F 0 "U5" H 6900 6000 50  0000 L CNN
F 1 "MotorDriver" H 6750 5900 50  0000 L CNN
F 2 "Footprints:MotorDriver" H 6950 6700 50  0001 C CNN
F 3 "" H 6950 6700 50  0001 C CNN
	1    6650 6750
	0    1    1    0   
$EndComp
Wire Wire Line
	2750 7450 2750 7500
Wire Wire Line
	2750 7500 3700 7500
Wire Wire Line
	3700 7500 3700 7450
Wire Wire Line
	3700 7500 4650 7500
Wire Wire Line
	4650 7500 4650 7450
Connection ~ 3700 7500
Wire Wire Line
	4650 7500 5600 7500
Wire Wire Line
	5600 7500 5600 7450
Connection ~ 4650 7500
Wire Wire Line
	5600 7500 6550 7500
Wire Wire Line
	6550 7500 6550 7450
Connection ~ 5600 7500
Wire Wire Line
	2650 7450 2650 7600
Wire Wire Line
	2650 7600 3600 7600
Wire Wire Line
	3600 7600 3600 7450
Wire Wire Line
	3600 7600 4550 7600
Wire Wire Line
	4550 7600 4550 7450
Connection ~ 3600 7600
Wire Wire Line
	4550 7600 5500 7600
Wire Wire Line
	5500 7600 5500 7450
Connection ~ 4550 7600
Wire Wire Line
	5500 7600 6450 7600
Wire Wire Line
	6450 7600 6450 7450
Connection ~ 5500 7600
Wire Wire Line
	6550 6650 6550 6550
Wire Wire Line
	6550 6550 5600 6550
Wire Wire Line
	5600 6550 5600 6650
Wire Wire Line
	5600 6550 4650 6550
Wire Wire Line
	4650 6550 4650 6650
Connection ~ 5600 6550
Wire Wire Line
	4650 6550 3700 6550
Wire Wire Line
	3700 6550 3700 6650
Connection ~ 4650 6550
Wire Wire Line
	3700 6550 2750 6550
Wire Wire Line
	2750 6550 2750 6650
Connection ~ 3700 6550
Wire Wire Line
	2650 6650 2650 6450
Wire Wire Line
	2650 6450 3600 6450
Wire Wire Line
	3600 6450 3600 6650
Wire Wire Line
	3600 6450 4550 6450
Wire Wire Line
	4550 6450 4550 6650
Connection ~ 3600 6450
Wire Wire Line
	4550 6450 5500 6450
Wire Wire Line
	5500 6450 5500 6650
Connection ~ 4550 6450
Wire Wire Line
	5500 6450 6450 6450
Wire Wire Line
	6450 6450 6450 6650
Connection ~ 5500 6450
$Comp
L power:GND #PWR03
U 1 1 5D97AF7C
P 6450 6450
F 0 "#PWR03" H 6450 6200 50  0001 C CNN
F 1 "GND" H 6455 6277 50  0000 C CNN
F 2 "" H 6450 6450 50  0001 C CNN
F 3 "" H 6450 6450 50  0001 C CNN
	1    6450 6450
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR04
U 1 1 5D97B4F1
P 6550 6550
F 0 "#PWR04" H 6550 6400 50  0001 C CNN
F 1 "+5V" H 6565 6723 50  0000 C CNN
F 2 "" H 6550 6550 50  0001 C CNN
F 3 "" H 6550 6550 50  0001 C CNN
	1    6550 6550
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5D97CBD8
P 6450 7600
F 0 "#PWR06" H 6450 7350 50  0001 C CNN
F 1 "GND" H 6455 7427 50  0000 C CNN
F 2 "" H 6450 7600 50  0001 C CNN
F 3 "" H 6450 7600 50  0001 C CNN
	1    6450 7600
	0    -1   -1   0   
$EndComp
Connection ~ 6450 7600
$Comp
L Teensy:Teensy3.5 U6
U 1 1 5D97EB9B
P 5400 3450
F 0 "U6" H 5400 6083 60  0000 C CNN
F 1 "Teensy3.5" H 5400 5977 60  0000 C CNN
F 2 "Teensy:Teensy35_36" H 5300 5700 60  0001 C CNN
F 3 "https://www.pjrc.com/teensy/card8a_rev2.pdf" H 5400 5871 60  0000 C CNN
F 4 "https://www.pjrc.com/teensy/pinout.html" H 5400 5773 50  0000 C CNN "Pinouts"
	1    5400 3450
	1    0    0    -1  
$EndComp
Text GLabel 2150 6350 1    50   Input ~ 0
PWM1
Text GLabel 3100 6350 1    50   Input ~ 0
PWM2
Text GLabel 4050 6350 1    50   Input ~ 0
PWM3
Text GLabel 5000 6350 1    50   Input ~ 0
PWM4
Text GLabel 5950 6350 1    50   Input ~ 0
PWM5
Text GLabel 2250 6350 1    50   Input ~ 0
INB1
Text GLabel 3200 6350 1    50   Input ~ 0
INB2
Text GLabel 4150 6350 1    50   Input ~ 0
INB3
Text GLabel 5100 6350 1    50   Input ~ 0
INB4
Text GLabel 6050 6350 1    50   Input ~ 0
INB5
Text GLabel 2350 6350 1    50   Input ~ 0
INA1
Text GLabel 3300 6350 1    50   Input ~ 0
INA2
Text GLabel 4250 6350 1    50   Input ~ 0
INA3
Text GLabel 5200 6350 1    50   Input ~ 0
INA4
Text GLabel 6150 6350 1    50   Input ~ 0
INA5
Text GLabel 2450 6350 1    50   Input Italic 0
CS1
Text GLabel 3400 6350 1    50   Input Italic 0
CS2
Text GLabel 5300 6350 1    50   Input Italic 0
CS4
Text GLabel 6250 6350 1    50   Input Italic 0
CS5
Text GLabel 6350 6350 1    50   Input ~ 0
EN5
Text GLabel 5400 6350 1    50   Input ~ 0
EN4
Text GLabel 4450 6350 1    50   Input ~ 0
EN3
Text GLabel 3500 6350 1    50   Input ~ 0
EN2
Text GLabel 2550 6350 1    50   Input ~ 0
EN1
Wire Wire Line
	2150 6650 2150 6350
Wire Wire Line
	2250 6350 2250 6650
Wire Wire Line
	2350 6650 2350 6350
Wire Wire Line
	2450 6350 2450 6650
Wire Wire Line
	2550 6650 2550 6350
Wire Wire Line
	3100 6350 3100 6650
Wire Wire Line
	3200 6650 3200 6350
Wire Wire Line
	3300 6350 3300 6650
Wire Wire Line
	3400 6650 3400 6350
Wire Wire Line
	3500 6350 3500 6650
Wire Wire Line
	4050 6650 4050 6350
Wire Wire Line
	4150 6350 4150 6650
Wire Wire Line
	4250 6650 4250 6350
Wire Wire Line
	4350 6350 4350 6650
Wire Wire Line
	4450 6650 4450 6350
Wire Wire Line
	5000 6650 5000 6350
Wire Wire Line
	5100 6350 5100 6650
Wire Wire Line
	5200 6650 5200 6350
Wire Wire Line
	5300 6350 5300 6650
Wire Wire Line
	5400 6650 5400 6350
Wire Wire Line
	5950 6350 5950 6650
Wire Wire Line
	6050 6650 6050 6350
Wire Wire Line
	6150 6350 6150 6650
Wire Wire Line
	6250 6650 6250 6350
Wire Wire Line
	6350 6350 6350 6650
Text GLabel 4250 1600 0    50   Input ~ 0
PWM1
Text GLabel 4250 1700 0    50   Input ~ 0
PWM2
Text GLabel 4250 1800 0    50   Input ~ 0
PWM3
Text GLabel 4250 1900 0    50   Input ~ 0
PWM4
Text GLabel 4250 2000 0    50   Input ~ 0
PWM5
Text GLabel 4250 5300 0    50   Input Italic 0
CS1
Text GLabel 4250 5400 0    50   Input Italic 0
CS2
Text GLabel 4250 5500 0    50   Input Italic 0
CS3
Text GLabel 4250 5600 0    50   Input Italic 0
CS4
Text GLabel 6550 5600 2    50   Input Italic 0
CS5
Text GLabel 4350 6350 1    50   Input Italic 0
CS3
Connection ~ 6550 6550
Connection ~ 6450 6450
Text GLabel 4250 2100 0    50   Input ~ 0
EN1
Text GLabel 4250 2200 0    50   Input ~ 0
EN2
Text GLabel 4250 2300 0    50   Input ~ 0
EN3
Text GLabel 4250 2400 0    50   Input ~ 0
EN4
Text GLabel 4250 2500 0    50   Input ~ 0
EN5
Text GLabel 4250 2800 0    50   Input ~ 0
INA1
Text GLabel 4250 2900 0    50   Input ~ 0
INA2
Text GLabel 4250 3000 0    50   Input ~ 0
INA3
Text GLabel 4250 3100 0    50   Input ~ 0
INA4
Text GLabel 4250 3200 0    50   Input ~ 0
INA5
Text GLabel 4250 4900 0    50   Input ~ 0
INB5
Text GLabel 4250 4800 0    50   Input ~ 0
INB4
Text GLabel 4250 4700 0    50   Input ~ 0
INB3
Text GLabel 4250 4600 0    50   Input ~ 0
INB2
Text GLabel 4250 4500 0    50   Input ~ 0
INB1
Wire Wire Line
	950  1350 1050 1350
Wire Wire Line
	1050 1350 1050 2050
Wire Wire Line
	1050 2050 950  2050
Wire Wire Line
	1050 2050 1050 2700
Wire Wire Line
	1050 2700 950  2700
Connection ~ 1050 2050
Wire Wire Line
	1050 2700 1050 3350
Wire Wire Line
	1050 3350 950  3350
Connection ~ 1050 2700
Wire Wire Line
	1050 3350 1050 4000
Wire Wire Line
	1050 4000 950  4000
Connection ~ 1050 3350
Wire Wire Line
	1050 4000 1050 4700
Wire Wire Line
	1050 4700 950  4700
Connection ~ 1050 4000
Wire Wire Line
	950  4400 1150 4400
Wire Wire Line
	1150 4400 1150 3700
Wire Wire Line
	1150 3700 950  3700
Wire Wire Line
	1150 3700 1150 3050
Wire Wire Line
	1150 3050 950  3050
Connection ~ 1150 3700
Wire Wire Line
	1150 3050 1150 2400
Wire Wire Line
	1150 2400 950  2400
Connection ~ 1150 3050
Wire Wire Line
	1150 2400 1150 1750
Wire Wire Line
	1150 1750 950  1750
Connection ~ 1150 2400
Wire Wire Line
	1150 1750 1150 850 
Wire Wire Line
	1150 850  950  850 
Connection ~ 1150 1750
Wire Wire Line
	1150 4400 1250 4400
Connection ~ 1150 4400
Wire Wire Line
	1050 4700 1250 4700
Connection ~ 1050 4700
$Comp
L power:+3V3 #PWR01
U 1 1 5D9D5676
P 1250 4400
F 0 "#PWR01" H 1250 4250 50  0001 C CNN
F 1 "+3V3" V 1265 4528 50  0000 L CNN
F 2 "" H 1250 4400 50  0001 C CNN
F 3 "" H 1250 4400 50  0001 C CNN
	1    1250 4400
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5D9D5E42
P 1250 4700
F 0 "#PWR02" H 1250 4450 50  0001 C CNN
F 1 "GND" V 1255 4572 50  0000 R CNN
F 2 "" H 1250 4700 50  0001 C CNN
F 3 "" H 1250 4700 50  0001 C CNN
	1    1250 4700
	0    -1   -1   0   
$EndComp
Text GLabel 1250 950  2    50   Input ~ 0
ELB1
Text GLabel 1250 1050 2    50   Input ~ 0
ELB2
Text GLabel 1250 1150 2    50   Input ~ 0
SHO1
Text GLabel 1250 1250 2    50   Input ~ 0
SHO2
Text GLabel 1250 1850 2    50   Input Italic 0
BASE1
Text GLabel 1250 2500 2    50   Input ~ 0
GRIP1
Text GLabel 1250 2600 2    50   Input ~ 0
GRIP2
Text GLabel 1250 3150 2    50   Input ~ 0
END1
Text GLabel 1250 3250 2    50   Input ~ 0
END2
Text GLabel 1250 3800 2    50   Input ~ 0
END3
Text GLabel 1250 3900 2    50   Input ~ 0
END4
Text GLabel 1250 4500 2    50   Input Italic 0
EXTRA1
Text GLabel 1250 4600 2    50   Input Italic 0
EXTRA2
Wire Wire Line
	950  950  1250 950 
Wire Wire Line
	950  1050 1250 1050
Wire Wire Line
	1250 1150 950  1150
Wire Wire Line
	950  1250 1250 1250
Wire Wire Line
	1250 1850 950  1850
NoConn ~ 950  1950
Wire Wire Line
	950  2500 1250 2500
Wire Wire Line
	950  2600 1250 2600
Wire Wire Line
	950  3150 1250 3150
Wire Wire Line
	1250 3250 950  3250
Wire Wire Line
	950  3800 1250 3800
Wire Wire Line
	1250 3900 950  3900
Wire Wire Line
	950  4500 1250 4500
Wire Wire Line
	950  4600 1250 4600
Text GLabel 4250 4200 0    50   Input Italic 0
BASE1
Text GLabel 4250 4300 0    50   Input Italic 0
EXTRA1
Text GLabel 4250 4400 0    50   Input Italic 0
EXTRA2
Text GLabel 4250 3300 0    50   Input ~ 0
ELB1
Text GLabel 4250 3400 0    50   Input ~ 0
ELB2
Text GLabel 4250 3500 0    50   Input ~ 0
SHO1
Text GLabel 4250 3600 0    50   Input ~ 0
SHO2
Text GLabel 6550 5500 2    50   Input ~ 0
END1
Text GLabel 6550 5400 2    50   Input ~ 0
END2
Text GLabel 6550 5300 2    50   Input ~ 0
END3
Text GLabel 6550 5200 2    50   Input ~ 0
END4
NoConn ~ 4250 3700
NoConn ~ 4250 3800
NoConn ~ 4250 3900
NoConn ~ 4250 4000
NoConn ~ 4250 4100
NoConn ~ 6550 4700
NoConn ~ 6550 4600
NoConn ~ 6550 4500
NoConn ~ 6550 4400
NoConn ~ 4250 5200
Text GLabel 4250 5000 0    50   Input ~ 0
GRIP1
Text GLabel 6550 5100 2    50   Input Italic 0
GRIP2
$Comp
L power:GND #PWR0101
U 1 1 5DA3B0AA
P 4250 1300
F 0 "#PWR0101" H 4250 1050 50  0001 C CNN
F 1 "GND" V 4255 1173 50  0000 R CNN
F 2 "" H 4250 1300 50  0001 C CNN
F 3 "" H 4250 1300 50  0001 C CNN
	1    4250 1300
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5DA3B1DB
P 4250 5100
F 0 "#PWR0102" H 4250 4850 50  0001 C CNN
F 1 "GND" V 4255 4973 50  0000 R CNN
F 2 "" H 4250 5100 50  0001 C CNN
F 3 "" H 4250 5100 50  0001 C CNN
	1    4250 5100
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5DA3BF93
P 6550 4900
F 0 "#PWR0103" H 6550 4650 50  0001 C CNN
F 1 "GND" V 6555 4772 50  0000 R CNN
F 2 "" H 6550 4900 50  0001 C CNN
F 3 "" H 6550 4900 50  0001 C CNN
	1    6550 4900
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR0104
U 1 1 5DA3D119
P 6550 5000
F 0 "#PWR0104" H 6550 4850 50  0001 C CNN
F 1 "+3V3" V 6565 5128 50  0000 L CNN
F 2 "" H 6550 5000 50  0001 C CNN
F 3 "" H 6550 5000 50  0001 C CNN
	1    6550 5000
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0105
U 1 1 5DA3D706
P 6550 4800
F 0 "#PWR0105" H 6550 4650 50  0001 C CNN
F 1 "+5V" V 6565 4928 50  0000 L CNN
F 2 "" H 6550 4800 50  0001 C CNN
F 3 "" H 6550 4800 50  0001 C CNN
	1    6550 4800
	0    1    1    0   
$EndComp
NoConn ~ 4250 1400
NoConn ~ 4250 1500
NoConn ~ 4250 2700
Text GLabel 4250 2600 0    50   Input ~ 0
LED
$Comp
L Connector_Generic:Conn_01x02 J7
U 1 1 5DA46C41
P 750 5150
F 0 "J7" H 668 4825 50  0000 C CNN
F 1 "12V_IN" H 668 4916 50  0000 C CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43650-0215_1x02_P3.00mm_Vertical" H 750 5150 50  0001 C CNN
F 3 "~" H 750 5150 50  0001 C CNN
	1    750  5150
	-1   0    0    1   
$EndComp
$Comp
L power:+12V #PWR0106
U 1 1 5DA47EBD
P 950 5050
F 0 "#PWR0106" H 950 4900 50  0001 C CNN
F 1 "+12V" V 965 5178 50  0000 L CNN
F 2 "" H 950 5050 50  0001 C CNN
F 3 "" H 950 5050 50  0001 C CNN
	1    950  5050
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5DA485D0
P 950 5150
F 0 "#PWR0107" H 950 4900 50  0001 C CNN
F 1 "GND" V 955 5022 50  0000 R CNN
F 2 "" H 950 5150 50  0001 C CNN
F 3 "" H 950 5150 50  0001 C CNN
	1    950  5150
	0    -1   -1   0   
$EndComp
$Comp
L Mosfet:IRLD110 Q1
U 1 1 5DA4CB18
P 2050 5600
F 0 "Q1" H 2500 5865 50  0000 C CNN
F 1 "IRLD110" H 2500 5774 50  0000 C CNN
F 2 "Footprints:DIP920W60P254L490H457Q4N" H 2800 5700 50  0001 L CNN
F 3 "https://docs-emea.rs-online.com/webdocs/09dd/0900766b809ddfde.pdf" H 2800 5600 50  0001 L CNN
F 4 "Transistor,MOSFET,IRLD110" H 2800 5500 50  0001 L CNN "Description"
F 5 "4.57" H 2800 5400 50  0001 L CNN "Height"
F 6 "Vishay" H 2800 5300 50  0001 L CNN "Manufacturer_Name"
F 7 "IRLD110" H 2800 5200 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "844-IRLD110" H 2800 5100 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=844-IRLD110" H 2800 5000 50  0001 L CNN "Mouser Price/Stock"
F 10 "3959359" H 2800 4900 50  0001 L CNN "RS Part Number"
F 11 "http://uk.rs-online.com/web/p/products/3959359" H 2800 4800 50  0001 L CNN "RS Price/Stock"
	1    2050 5600
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J8
U 1 1 5DA52CD3
P 750 5600
F 0 "J8" H 668 5275 50  0000 C CNN
F 1 "LED" H 668 5366 50  0000 C CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43650-0215_1x02_P3.00mm_Vertical" H 750 5600 50  0001 C CNN
F 3 "~" H 750 5600 50  0001 C CNN
	1    750  5600
	-1   0    0    1   
$EndComp
Wire Wire Line
	1150 5700 1150 5600
Wire Wire Line
	950  5600 1150 5600
Connection ~ 1150 5600
Text GLabel 2050 5600 2    50   Input ~ 0
LED
$Comp
L power:GND #PWR0108
U 1 1 5DA61015
P 2050 5700
F 0 "#PWR0108" H 2050 5450 50  0001 C CNN
F 1 "GND" V 2055 5572 50  0000 R CNN
F 2 "" H 2050 5700 50  0001 C CNN
F 3 "" H 2050 5700 50  0001 C CNN
	1    2050 5700
	0    -1   -1   0   
$EndComp
$Comp
L Regulator_Switching:TSR_1-2450 U7
U 1 1 5DA61D78
P 2800 2400
F 0 "U7" H 2800 2767 50  0000 C CNN
F 1 "TSR_1-2450" H 2800 2676 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_TRACO_TSR-1_THT" H 2800 2250 50  0001 L CIN
F 3 "http://www.tracopower.com/products/tsr1.pdf" H 2800 2400 50  0001 C CNN
	1    2800 2400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5DA62DFE
P 2800 2700
F 0 "#PWR0109" H 2800 2450 50  0001 C CNN
F 1 "GND" H 2805 2527 50  0000 C CNN
F 2 "" H 2800 2700 50  0001 C CNN
F 3 "" H 2800 2700 50  0001 C CNN
	1    2800 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 2700 2800 2600
$Comp
L Device:CP C2
U 1 1 5DA666E3
P 3300 2550
F 0 "C2" H 3418 2596 50  0000 L CNN
F 1 "CP" H 3418 2505 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 3338 2400 50  0001 C CNN
F 3 "~" H 3300 2550 50  0001 C CNN
	1    3300 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 5DA66D8A
P 2300 2550
F 0 "C1" H 2418 2596 50  0000 L CNN
F 1 "CP" H 2418 2505 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2338 2400 50  0001 C CNN
F 3 "~" H 2300 2550 50  0001 C CNN
	1    2300 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 2700 2300 2700
Connection ~ 2800 2700
Wire Wire Line
	2300 2400 2300 2300
Wire Wire Line
	2300 2300 2400 2300
Wire Wire Line
	3200 2300 3300 2300
Wire Wire Line
	3300 2300 3300 2400
Wire Wire Line
	3300 2700 2800 2700
$Comp
L power:+12V #PWR0110
U 1 1 5DA75343
P 2300 2300
F 0 "#PWR0110" H 2300 2150 50  0001 C CNN
F 1 "+12V" H 2315 2473 50  0000 C CNN
F 2 "" H 2300 2300 50  0001 C CNN
F 3 "" H 2300 2300 50  0001 C CNN
	1    2300 2300
	1    0    0    -1  
$EndComp
Connection ~ 2300 2300
$Comp
L power:+5V #PWR0111
U 1 1 5DA759A4
P 3300 2300
F 0 "#PWR0111" H 3300 2150 50  0001 C CNN
F 1 "+5V" H 3315 2473 50  0000 C CNN
F 2 "" H 3300 2300 50  0001 C CNN
F 3 "" H 3300 2300 50  0001 C CNN
	1    3300 2300
	1    0    0    -1  
$EndComp
Connection ~ 3300 2300
$Comp
L power:+12V #PWR0112
U 1 1 5DA7F3F4
P 950 5500
F 0 "#PWR0112" H 950 5350 50  0001 C CNN
F 1 "+12V" V 965 5628 50  0000 L CNN
F 2 "" H 950 5500 50  0001 C CNN
F 3 "" H 950 5500 50  0001 C CNN
	1    950  5500
	0    1    1    0   
$EndComp
$Comp
L power:+BATT #PWR0113
U 1 1 5DA8A9E9
P 6550 7500
F 0 "#PWR0113" H 6550 7350 50  0001 C CNN
F 1 "+BATT" V 6565 7628 50  0000 L CNN
F 2 "" H 6550 7500 50  0001 C CNN
F 3 "" H 6550 7500 50  0001 C CNN
	1    6550 7500
	0    1    1    0   
$EndComp
Connection ~ 6550 7500
$Comp
L Connector_Generic:Conn_01x02 J9
U 1 1 5DA8AA7E
P 750 6100
F 0 "J9" H 668 5775 50  0000 C CNN
F 1 "BATT_IN" H 668 5866 50  0000 C CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43650-0215_1x02_P3.00mm_Vertical" H 750 6100 50  0001 C CNN
F 3 "~" H 750 6100 50  0001 C CNN
	1    750  6100
	-1   0    0    1   
$EndComp
$Comp
L power:+BATT #PWR0114
U 1 1 5DA8B534
P 950 6000
F 0 "#PWR0114" H 950 5850 50  0001 C CNN
F 1 "+BATT" V 965 6128 50  0000 L CNN
F 2 "" H 950 6000 50  0001 C CNN
F 3 "" H 950 6000 50  0001 C CNN
	1    950  6000
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5DA8BF9C
P 950 6100
F 0 "#PWR0115" H 950 5850 50  0001 C CNN
F 1 "GND" V 955 5972 50  0000 R CNN
F 2 "" H 950 6100 50  0001 C CNN
F 3 "" H 950 6100 50  0001 C CNN
	1    950  6100
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
